import argparse
import os

import soundfile


__docstring__ = """
Detect peaks in a stereo audio file.

This utility will scan both channels of a stereo audio file, detecting the sample indexes
of all of the transient peaks greater than a user-input threshold value.
A CSV output of the result is generated and placed in the same location as the source file.

Mono files throw an exception.
"""


def parse_args():
    ap = argparse.ArgumentParser(description=__docstring__)

    ap.add_argument('threshold',
                    type=float,
                    help="Threshold value to determine peaks: " +
                    "range is 0-1, with 1 being full scale output")

    ap.add_argument('filepath',
                    help="Path of the file to analyse.")

    return ap.parse_args()


def detect_sample_peaks(sample_data, sample_rate, threshold):
    peaks = [list(), list()]
    # length of time to stop detecting after the first peak is found
    # We wait 10 milliseconds to ensure a clean detection.
    # This is only going to be effective on a short lived sample transient.
    window = (sample_rate / 1000.0) * 10
    # cache the last detected peak position per channel
    last_indexes = []

    for index, frame in enumerate(sample_data):
        for channel, channel_amplitude in enumerate(frame):
            if channel_amplitude > threshold and index > last_indexes[channel] + window:
                last_indexes[channel] = index
                peaks[channel].append(index)

    return peaks


def format_csv_string(*data):
    if len(data) is not 2 or len(data[1]) is 0:
        raise IndexError("The data has to have a minimum of 2 columns.")

    # column headers
    csv_str = ['left channel,right channel\n']
    # append the column values
    for col1val, col2val in zip(data[0], data[1]):
        csv_str.append(str(col1val)+','+str(col2val)+'\n')

    return csv_str


def write_to_csv(data, path):
    if not list(data):
        raise TypeError("data must be an iterable container.")

    if data is 0:
        raise ValueError("data container is empty!")

    with open(os.path.join(path, 'peak_results.csv'), 'w+') as result_file:
        result_file.writelines(data)


def main():
    args = parse_args()

    cwd = os.path.dirname(os.path.abspath(__file__))
    data, fs = soundfile.read(os.path.join(cwd, args.filepath))

    peak_array = detect_sample_peaks(data, fs, args.threshold)

    csv_string = format_csv_string(peak_array[0], peak_array[1])
    write_to_csv(csv_string, cwd)


if __name__ == '__main__':
    main()
